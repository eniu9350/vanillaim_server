package main

import (
	"log"
	"net"
	"strings"
)

type ConnMgr struct {
	//	DataMgr DataMgr
	ConnMap map[string]net.Conn //<fullid, connection> map,
}

func NewConnMgr() ConnMgr {
	connMgr := ConnMgr{}
	connMgr.ConnMap = make(map[string]net.Conn)
	return connMgr
}

//TODO: same as set currently
func (connMgr *ConnMgr) add(uid string, did string, conn net.Conn) {
	key := uid
	//	if user.devid != ""{
	key += ":" + did //TODO: append ":" if devid is empty string, make sure this is right
	//	}
	connMgr.ConnMap[key] = conn
}

func (connMgr *ConnMgr) set(uid string, did string, conn net.Conn) {
	key := uid
	//	if user.devid != ""{
	key += ":" + did //TODO: append ":" if devid is empty string, make sure this is right
	//	}
	connMgr.ConnMap[key] = conn
}

func (connMgr *ConnMgr) remListByUid(uid string) int {
	i := 0
	for k := range connMgr.ConnMap {
		if strings.HasPrefix(k, uid+":") {
			delete(connMgr.ConnMap, k)
			i++
		}
	}
	return i
}

//TODO: temp!!!
func (connMgr *ConnMgr) getRandomByUid(uid string) net.Conn {
	log.Printf("[choose] uid=%s, p of connMgr=%p", uid, connMgr.ConnMap)
	for k := range connMgr.ConnMap {
		//		log.Printf("%s = %s", k, msg.Body.Props[k])
		if strings.HasPrefix(k, uid+":") {
			return connMgr.ConnMap[k]
		}
	}
	return nil
}
