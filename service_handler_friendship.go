package main

// import
import (
	"net"
	"fmt"
	lib "realjin.info/vanillaim_lib"
	domain "realjin.info/vanillaim_lib/domain"
)


func (serviceHandler *DefaultServiceHandler) doFriendshipAdd(msg *lib.Msg, conn net.Conn) {
	strRequester := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_REQUESTER)
	responser := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_RESPONSER)
	requester := domain.ParseQName(strRequester)

	sessionResponser := serviceHandler.SessionMgr.getSessionByUserName(responser)

	//TODO: ! other cases
	serviceHandler.FriendshipDao.FriendshipRequestCreate(requester.UserName, responser)

	//add nickname
	user, err := serviceHandler.UserDao.UserGet(requester.UserName)
	if err == nil	{
		Log.Info("nickname=============>"+user.Nickname)
		if sessionResponser == nil	{
			Log.Info("sessionResponser nil")
		} else{
			Log.Info("sessionResponser not nil")
		}
		msg.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_NICKNAME] = user.Nickname
		//forward to responser
		fmt.Fprintf(sessionResponser.Conn, msg.ToString())
	}else{
		Log.Info("UserGet error in doFriendshipAdd.")
		//TODO exception handling
	}
}

func (serviceHandler *DefaultServiceHandler) doFriendshipAddFin(msg *lib.Msg, conn net.Conn) {
	strRequester := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_REQUESTER)
	responser := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_RESPONSER)
	requester := domain.ParseQName(strRequester)

	Log.Info("===============> in doFriendshipAddFin, r.un="+requester.UserName)

	sessionRequester := serviceHandler.SessionMgr.getSessionByUserName(requester.UserName)

	//TODO: ! other cases

	//update request in cache
	// serviceHandler.FriendshipDao.FriendshipRequestUpdate(requester.UserName, responser)

	//add friend
	serviceHandler.FriendshipDao.FriendshipCreate(requester.UserName, responser)

	//send update response
	response := lib.NewMsg()
	response.Header.Verb = lib.MSGHEADER_VERB_FUPDATE
	response.Body.Props[lib.MSGBODY_MVPROPS_BASIC_KEY_MEMBER] = lib.MSGBODY_PROPS_VALUE_ELEMENT_OF_MEMBER_ACTION_ADD + lib.MSGBODY_PROPS_VALUE_SEPARATOR + responser

	fmt.Fprintf(sessionRequester.Conn, response.ToString())
}
