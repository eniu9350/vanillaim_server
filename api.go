package main

import (
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"github.com/martini-contrib/cors"
	rtdb "github.com/dancannon/gorethink"
	rtdao "realjin.info/vanillaim_lib/dao/rethinkdb"
	dao "realjin.info/vanillaim_lib/dao"
	"strconv"
	"fmt"
	"net/http"
	// . "realjin.info/vanillaim_server/api"
)

type Api struct	{
	Port int
	SessionMgr *SessionMgr
	UserDao dao.UserDao
	FriendshipDao dao.FriendshipDao
	MartiniServer *martini.ClassicMartini
}

func NewApi(cfg *ServerConfig, sessionMgr* SessionMgr) *Api	{
	api := Api{}
	api.Port = cfg.Api.Port
	api.SessionMgr = sessionMgr

	var rtdbSession *rtdb.Session
	//establish rethinkdb connection
	rtdbSession, err := rtdb.Connect(rtdb.ConnectOpts{
		Address: cfg.Db.Address})  //188.166.253.239
	if err != nil {
		Log.Error(err.Error())
	}
	userDao := rtdao.NewUserDaoRethinkDB(rtdbSession)
	friendshipDao := rtdao.NewFriendshipDaoRethinkDB(rtdbSession)

	api.UserDao = &userDao
	api.FriendshipDao = &friendshipDao

	return &api
}

func (api *Api)Start()	{
	m := martini.Classic()

	api.MartiniServer = m

	api.routeUserAndSessionApi()
	api.routeFriendshipApi()

	//middleware init
	m.Use(render.Renderer())
	m.Use(cors.Allow(&cors.Options{
	 AllowOrigins:     []string{"http://localhost:5020"},
	 AllowMethods:     []string{"GET", "POST"},
	 AllowHeaders:     []string{"Origin"},
	 ExposeHeaders:    []string{"Content-Length"},
	 AllowCredentials: true,
 }))

	fmt.Println("api port="+strconv.Itoa(api.Port))
	m.RunOnAddr("0.0.0.0:"+strconv.Itoa(api.Port))
}

/* ============================ session ============================ */
func (api *Api)routeUserAndSessionApi(){
	api.MartiniServer.Get("/getUsers", func(r render.Render) {
		users, _ := api.UserDao.UserGetAll();
		//TODO: temp!!
		apiUsers := make([]ApiDomainUser, 0, 50)
		for _,user := range users	{
			apiUser := ApiDomainUser{}
			apiUser.Username = user.Username
			apiUser.Password = user.Password
			if api.SessionMgr.getSessionByUserName(user.Username)!=nil	{
				apiUser.Status = 1;
			}else{
				apiUser.Status = 2;
			}
			apiUsers = append(apiUsers, apiUser)
		}

		r.JSON(200, apiUsers)
	});

	api.MartiniServer.Get("/getUser", func(req *http.Request, r render.Render) {
		v := req.URL.Query()
		//TODO: check
		userName := v["userName"][0]

		session := api.SessionMgr.getSessionByUserName(userName)
		if session!=nil	{
			Log.Info("session not nil")
			r.JSON(200, session)
		}	else	{

		}
	});
}

func (api *Api)routeFriendshipApi(){
	api.MartiniServer.Get("/getFriendships", func(req *http.Request, r render.Render) {
		v := req.URL.Query()
		//TODO: check
		userName := v["userName"][0]

		friendships, _ := api.FriendshipDao.FriendshipGetAll(userName);
		//TODO: temp!!
		apiFriendships := make([]ApiDomainFriendship, 0, 50)
		for _,friendship := range friendships	{
			apiFriendship := ApiDomainFriendship{}
			apiFriendship.UserName = friendship.UserName
			apiFriendship.Friend = friendship.Friend
			apiFriendships = append(apiFriendships, apiFriendship)
		}

		r.JSON(200, apiFriendships)
	});

}
