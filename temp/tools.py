import datetime
import os
import json

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


wc = {}
wc['date'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

# ----------  lib ------------------------
wc['serverlib'] = {}
wc['serverlib']['total'] = 0
wc['serverlib']['go'] = 0

dir = "../../vanillaim_lib"
for root, dirs, files in os.walk(dir):
    path = root.split('/')
    # print (len(path) - 1) *'---' , os.path.basename(root)
    for file in files:
        # print file+', dir=',root
        if file.endswith(".go"):
            wc['serverlib']['go'] = wc['serverlib']['go'] + file_len(root+"/"+file)
        # print len(path)*'---', file

print "wc['serverlib']['go']=",wc['serverlib']['go']

# ----------  server ------------------------
wc['server'] = {}
wc['server']['total'] = 0
wc['server']['go'] = 0
wc['server']['js'] = 0

dir = os.path.abspath("../")
for file in os.listdir(dir):
    if file.endswith(".go"):
        wc['server']['go'] = wc['server']['go'] + file_len(os.path.join(dir, file))

dir = os.path.abspath("../console/js")
for root, dirs, files in os.walk(dir):
    for name in files:
        file = os.path.join(root, name)
        if file.endswith(".js") and file.find("jquery")==-1 and file.find("/dist/")==-1 and file.find("bundle")==-1:
            wc['server']['js'] = wc['server']['js'] + file_len(file)

print "wc['server']['go']=",wc['server']['go']
print "wc['server']['js']=",wc['server']['js']


# ----------  elclient ------------------------
wc['elclient'] = {}
wc['elclient']['total'] = 0
wc['elclient']['js_jsx'] = 0

dir = "/home/jinreal/prog/electronworkspace/vanillaim_elclient/app"
for root, dirs, files in os.walk(dir):
    path = root.split('/')
    # print (len(path) - 1) *'---' , os.path.basename(root)
    for file in files:
        # print file+', dir=',root
        wc['elclient']['js_jsx'] = wc['elclient']['js_jsx'] + file_len(root+"/"+file)
        # print len(path)*'---', file

print "wc['elclient']['js_jsx']=",wc['elclient']['js_jsx']

# ----------  summary ------------------------
wc['summary'] = {}
wc['summary']['go'] = wc['serverlib']['go'] + wc['server']['go']
wc['summary']['js_jsx'] = wc['server']['js'] + wc['elclient']['js_jsx']

print "wc['summary']['go']=",wc['summary']['go']
print "wc['summary']['js_jsx']=",wc['summary']['js_jsx']

with open('./wc.txt', 'a') as f:
    f.write(json.dumps(wc)+'\r\n')
