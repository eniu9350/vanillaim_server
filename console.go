package main

import (
	// "fmt"
	// rdb "github.com/dancannon/gorethink"
	"github.com/go-martini/martini"
	// "github.com/go-martini/martini/binding"
	// "github.com/martini-contrib/binding"
	"github.com/martini-contrib/render"
	"strconv"
	"fmt"
	// log "github.com/Sirupsen/logrus"
)

/*
solely rely on persisted data?
*/
//TODO: consequence of providing json in datalayer?

// type UserForm struct {
// 	Username string `form:"username" binding:"required"`
// 	Password string `form:"password" binding:"required"`
// }

type Console	struct {
	Port int;
}

func NewConsole(cfg *ServerConfig) *Console{
	console := Console{}
	console.Port = cfg.Console.Port
	return &console
}

func (console *Console) Start() {
	//martini init
	m := martini.Classic()

	//middleware init
	m.Use(render.Renderer())

	m.Get("/", func() string {
		return "Hello world!"
	})

	// m.Post("/resetPsw", binding.Bind(UserForm{}), func(user UserForm) string {
	// 	session, err := rdb.Connect(rdb.ConnectOpts{
	// 		Address: "188.166.253.239:28015"})
	// 	if err != nil {
	// 		log.Fatalln(err.Error())
	// 	}
	//
	// 	res, err := rdb.DB(lib.RTDB_DBNAME).Table(lib.RTDB_TABLENAME_USER).GetAllByIndex("username", user.Username).Run(session)
	// 	var row lib.UserModel
	// 	//TODO: no check of multiple rows?
	// 	err = res.One(&row)
	//
	// 	_, errUpdate := rdb.DB(lib.RTDB_DBNAME).Table(lib.RTDB_TABLENAME_USER).Get(row.Id).Update(map[string]interface{}{
	// 		"password": user.Password,
	// 	}, rdb.UpdateOpts{
	// 		ReturnChanges: true,
	// 	}).Run(session) //or RunWrite?
	//
	// 	if errUpdate == nil {
	// 		return "ok"
	// 	} else {
	// 		return "error"
	// 	}
	// })
	//
	// m.Get("/getUsers", func(r render.Render) {
	// 	session, err := rdb.Connect(rdb.ConnectOpts{
	// 		Address: "188.166.253.239:28015"})
	// 	if err != nil {
	// 		log.Fatalln(err.Error())
	// 	}
	//
	// 	// rdb.DB("database").Table("table").Get("GUID").Run(session)
	// 	res, err := rdb.DB("vnim").Table("users").Run(session)
	//
	// 	var row lib.UserModel
	// 	// users := []lib.UserModel{}
	// 	users := []User{}
	// 	for res.Next(&row) {
	// 		// fmt.Printf(row)
	// 		fmt.Printf("row got")
	// 		// fmt.Printf(row.Username)
	//
	// 		presence := lib.DbopPresenceGet(redisConn, row.Username)
	// 		u := User{}
	// 		u.Username = row.Username
	// 		u.Presence = presence
	// 		users = append(users, u)
	// 		// Do something with row
	// 	}
	//
	// 	if res.Err() != nil {
	// 		// error
	// 		fmt.Print(err)
	// 	}
	//
	// 	// r.JSON(200, map[string]interface{}{"hello": "world"})
	// 	r.JSON(200, users)
	// })

	m.Use(martini.Static("console"))

	fmt.Println("console port="+strconv.Itoa(console.Port))
	m.RunOnAddr("0.0.0.0:"+strconv.Itoa(console.Port))
}
