package main

import  (
  "fmt"
  "io/ioutil"
  "gopkg.in/yaml.v2"
)

type ServerConfig struct {
	Server struct {
		Port int
	}
  Api	struct	{
		Port int
	}
	Console	struct	{
		Port int
	}
  Db struct {
    Name string
    Address string
  }
	Log struct {
		Info string
		Error string
	}
}

func ParseConfig() *ServerConfig	{
	//parse config file
	cfg := ServerConfig{}

	cfgBytes, err := ioutil.ReadFile("conf/vanillaim.yml")

	err = yaml.Unmarshal(cfgBytes, &cfg)
	if err != nil {
					// log.Fatalf("error: %v", err)
	}
	fmt.Printf("--- cfg:\n%v\n\n", cfg)

	return &cfg
}
