var React = require('react');
var Griddle = require('griddle-react');

var UserListColumnMeta = require('./userlist-content/column-meta.js')

var UserListContent = React.createClass({
  getInitialState: function() {
    return {}
  },
  render: function() {
    $('#bt-onlineuser-detail-close').click(function() {
      var modal = UIkit.modal("#onlineuser-detail")
      modal.hide()
    })

    return (
      <div>
        <Griddle results={this.props.data} columnMetadata={UserListColumnMeta} columns={["Username", "Status", "Operation"]}/>
      </div>
    )
  }
})

module.exports = UserListContent
