var React = require('react');
var Griddle = require('griddle-react');

var FriendshipListColumnMeta = require('./friendshiplist-content/column-meta.js')
var Menus = require('../menus.js')

var FriendshipListContent = React.createClass({
  getInitialState: function() {
    return {}
  },

  returnHome: function() {
    var menu
    for(var i=0;i<Menus['data'].length;i++){
      if(Menus['data'][i]['id']==2){
        menu = Menus['data'][i]
        this.props.dispatcher.dispatch({
          eventName: 'menuClick',
          menu: menu
        })
      }
    }
  },

  render: function() {
    return (
      <div>
        <div>head</div>
        <Griddle results={this.props.data} columnMetadata={FriendshipListColumnMeta} columns={["Friend", "Operation"]}/>
        <div>
          <a onClick={this.returnHome}>Back</a>
        </div>
      </div>
    );
  }
})

module.exports = FriendshipListContent
