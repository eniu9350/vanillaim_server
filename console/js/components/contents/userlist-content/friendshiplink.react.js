var React = require('react')

var FriendshipListContent = require('../friendshiplist-content.react.js')

var FriendshipLink = React.createClass({
  click: function() {
    var username = this.props.rowData.Username
    var me = this
    $.get("http://localhost:5006/getFriendships", {userName: username}).done(function(data) {
      console.log('in getFriendships');
      console.log(data);
      React.render(React.createElement(FriendshipListContent, Object.assign({}, this.props, {
        data: data,
        dispatcher: me.props.rowData['dispatcher']
      })), document.getElementById('content'));
    })

  },
  render: function() {
    return <a onClick={this.click}>Edit friendship</a>
  }
})

module.exports = FriendshipLink
