// var FriendshipLink = require('./friendshiplink.react.js')
var StatusColumn = require('./statuscolumn.react.js')
var OperationColumn = require('./operationcolumn.react.js')

var ColumnMeta = [
  {
    "columnName": "Username",
    "order": 1,
    "locked": false,
    "visible": true,
    "displayName": "Name"
  }, {
    "columnName": "Status",
    "order": 2,
    "locked": false,
    "visible": true,
    "displayName": "Status",
    "customComponent": StatusColumn
  }, {
    "columnName": "Password",
    "order": 3,
    "locked": false,
    "visible": true
  }, {
    "columnName": "UserId",
    "order": 4,
    "locked": false,
    "visible": true
  }, {
    "columnName": "Operation",
    "order": 5,
    "locked": false,
    "visible": true,
    "displayName": "Operation",
    "customComponent": OperationColumn
  }
]

module.exports = ColumnMeta
