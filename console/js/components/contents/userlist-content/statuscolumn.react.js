var React = require('react')

var StatusLink = React.createClass({
  click: function() {
    var userName = this.props.rowData.UserName
    $.get("http://localhost:5006/getUser", {userName: userName}).done(function(data) {
      var modal = UIkit.modal("#onlineuser-detail");
      $("#onlineuser-detail-title").text('User "' + userName + '"')

      var content = []
      content.push("<p>create time: " + (new Date(parseInt(data.CreateTime))).customFormat("#DD#/#MM#/#YYYY# #hh#:#mm#:#ss#") + "</p>")
      content.push("<p>update time: " + (new Date(parseInt(data.UpdateTime))).customFormat("#DD#/#MM#/#YYYY# #hh#:#mm#:#ss#") + "</p>")

      $('#onlineuser-detail-content').html(content.join(''))
      // onlineuser-detail-content
      if (modal.isActive()) {
        modal.hide()
      } else {
        modal.show()
      }
    });
  },
  render: function() {
    // url ="speakers/" + this.props.rowData.state + "/" + this.props.data;
    url = "#onlineuser-detail"
    status = this.props.data
    if (status == 1) {
      return <a onClick={this.click}>Online</a>
    } else if (status == 2) {
      return <span>Offline</span>
    }
  }
})

module.exports = StatusLink
