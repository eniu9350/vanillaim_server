var React = require('react')

var FriendshipLink = require('./friendshiplink.react.js')

var OperationColumn = React.createClass({
  render: function(){
    return(
      <FriendshipLink data={this.props.data} rowData={this.props.rowData}></FriendshipLink>
    )
  }
})

module.exports = OperationColumn
