var OperationColumn = require('./operationcolumn.react.js')

var ColumnMeta = [
  {
    "columnName": "Friend",
    "order": 1,
    "locked": false,
    "visible": true,
    "displayName": "Name"
  },{
    "columnName": "Operation",
    "order": 2,
    "locked": false,
    "visible": true,
    "displayName": "Op",
    "customComponent": OperationColumn
  }
]

module.exports = ColumnMeta
