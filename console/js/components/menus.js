var Menus = {
  'data': [{
    // 'li_id':'navigator-menu-1',
    'id': 1,
    'text': 'Overview(N/A)'
  }, {
    // 'li_id':'navigator-menu-2',
    'id': 2,
    'text': 'Users',
    'action': 'ShowUserList'
  }, {
    // 'li_id':'navigator-menu-2',
    'id': 3,
    'text': 'Online(N/A)'
  }, {
    // 'li_id':'navigator-menu-2',
    'id': 4,
    'text': 'FriendList(N/A)',
    'action': 'ShowFriendshipList'
  }]
}

module.exports = Menus
