var React = require('react');

var ListItemWrapper = React.createClass({
  switchContent: function() {
    this.props.clickMenu(this.props.data);
  },
  render: function() {
    var id = 'navigator-menu'+this.props.data.id;
    return <li id={id} onClick={this.switchContent}><a href="#">{this.props.data.text}</a></li>;
  }
});

var Navigator = React.createClass({
  clickMenu(menu) {
    this.props.dispatcher.dispatch({
      eventName: 'menuClick',
      menu: menu
    });
  },
  render: function(){
    var me = this;
    return (
      <ul className="uk-nav">
        {this.props.menu.map(function(menu) {
           return <ListItemWrapper clickMenu={me.clickMenu} data={menu} key={menu.id}/>;
        })}
      </ul>
    );
  }
});

module.exports = Navigator;
