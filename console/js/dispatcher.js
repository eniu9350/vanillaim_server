var React = require('react')
var FluxDispatcher = require('flux').Dispatcher
var UserListContent = require('./components/contents/userlist-content.react.js')

function Dispatcher() {
  var dispatcher = new FluxDispatcher();

  dispatcher.register(function(payload) {
    console.log('in ' + payload.eventName + ' event')
    if (payload.eventName == 'menuClick') {
      var action = payload.menu['action']
      if (action == 'ShowUserList') {
        $.get("http://localhost:5006/getUsers", function(data) {
          var dataWithDispatcher = data
          for (var i = 0; i < dataWithDispatcher.length; i++) {
            dataWithDispatcher[i]['dispatcher'] = dispatcher
          }
          React.render(React.createElement(UserListContent, Object.assign({}, this.props, {
            data: dataWithDispatcher,
            dispatcher: dispatcher
          })), document.getElementById('content'));
          // React.render(React.createElement(Navigator, Object.assign({}, this.props, {menus: menus, dispatcher: dispatcher})), document.getElementById('content'));
        });
      } else if (action == 'ShowFriendshipList') {
        $.get("http://localhost:5006/getFriendships", {userName: 'user1'}).done(function(data) {
          console.log('in getFriendships');
          console.log(data);
        });
        var modal = UIkit.modal("#friendship-list");
        if (modal.isActive()) {
          modal.hide()
        } else {
          modal.show()
        }
      }
    } //end if (payload.eventName == 'menuClick')

    return true; // Needed for Flux promise resolution
  });

  return dispatcher
}

module.exports = Dispatcher
