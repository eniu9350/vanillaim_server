var React = require('react');
var Griddle = require('griddle-react');

var Navigator = require('./components/navigator.react.js');
var Dispatcher = require('./dispatcher.js')
var Util = require('./util.js')
var $ = require('jquery-browserify')
var UIKit = require('uikit')  //load uikit javascript lib for usage in plain html

var Menus = require('./components/menus.js')

//util init
Util.init()

var dispatcher = new Dispatcher();

React.render(React.createElement(Navigator, Object.assign({}, this.props, {menu: Menus.data, dispatcher: dispatcher})), document.getElementById('navigator'));
