// var gulp = require('gulp');
// var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
// var react = require('gulp-react');
// var htmlreplace = require('gulp-html-replace');
//
// var path = {
//   HTML: 'src/index.html',
//   ALL: ['src/js/*.js', 'src/js/**/*.js', 'src/index.html'],
//   JS: ['js/*.js'],
//   MINIFIED_OUT: 'build.min.js',
//   DEST_SRC: 'js/dist',
//   DEST_BUILD: 'dist/build',
//   DEST: 'dist'
// };
//
// gulp.task('transform', function(){
//   gulp.src(path.JS)
//     .pipe(react())
//     .pipe(concat(path.MINIFIED_OUT))
//     .pipe(uglify(path.MINIFIED_OUT))
//     .pipe(gulp.dest(path.DEST_SRC));
// });

//-------------2 ---------



// gulp.task("browserify", function () {
//     var b = browserify({
//         entries: "./javascripts/src/main.js",
//         debug: true
//     });
//
//     return b.bundle()
//         .pipe(source("bundle.js"))
//         .pipe(buffer())
//         .pipe(sourcemaps.init({loadMaps: true}))
//         .pipe(sourcemaps.write("."))
//         .pipe(gulp.dest("./javascripts/dist"));
// });

// ------------- 3 ------------
var browserify = require('browserify');
var gulp = require('gulp');
var source = require("vinyl-source-stream");
var reactify = require('reactify');

gulp.task('browserify', function(){
  var b = browserify();
  b.transform('reactify',{harmony:true, es6module:true}); // use the reactify transform
  b.add('./js/app.js');
  return b.bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./js/dist'));
});
