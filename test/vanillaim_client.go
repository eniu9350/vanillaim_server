package other

import (
	"bufio"
	"fmt"
	"os"
	"log"
	"net"
	"strconv"
	. "realjin.info/vanillaim_lib"
)


const CONSOLE_DISPLAY_WELCOME = "Message sending console started, a line of single '.' will end the conversation."
const CONSOLE_DISPLAY_INPUT_TO = "To whom?"
const CONSOLE_DISPLAY_INPUT_FROM = "From whom?"
const CONSOLE_DISPLAY_INPUT_CONTENT = "Content is?"

type VanillaImClient struct {
}

/*
* arg 1: hostname (required)
* arg 2: port (optional)
*
*
 */
func main() {
	argsWithoutProg := os.Args[1:]

	lenOfArgs := len(argsWithoutProg)

	port := -1

	if lenOfArgs > 2 {
		log.Fatalf("There should be maximum of 2 paramters!")
		return
	}

	if lenOfArgs == 1 {
		port = 5005
	} else if lenOfArgs == 2 {
		port, _ = strconv.Atoi(os.Args[2])
		//TODO check validity of port
	}

	hostName := os.Args[1]

	log.Printf("host name=%s, port=%d", hostName, port)

	//connect
	conn, _ := net.Dial("tcp", fmt.Sprintf("%s:%d", hostName, port))

	go receive(conn)


	//TODO: temp
	dummyFunc(port, hostName)

	// for {
	//   send(to, content)
	// }

	scanner := bufio.NewScanner(os.Stdin)
	i := 0

	to := string("")
	from := string("")
	content := string("")

	log.Printf(CONSOLE_DISPLAY_WELCOME)
	log.Printf(CONSOLE_DISPLAY_INPUT_FROM)
	for scanner.Scan() {
		line := scanner.Text()

		if i == 0 { //odd round
			//start handling next message
			from = line

			log.Println("from len = %d", len(from))

			log.Printf(CONSOLE_DISPLAY_INPUT_TO)

		} else if (i == 1) {
			to = line

			//send message if it is valid
			//			send(conn, to, content)
			log.Println("to len = %d", len(to))

			log.Printf(CONSOLE_DISPLAY_INPUT_CONTENT)
		}else if (i == 2) {
			content = line

			//send message if it is valid
			send(conn, from, to, content)
			log.Println("content len = %d", len(content))

			log.Printf(CONSOLE_DISPLAY_INPUT_FROM)
		}
		i = (i + 1) % 3


		if line == "." {
			break
		}
		// fmt.Println(line) // or do something else with line


		// fmt.Fprintf(conn, text + "\n")     // listen for reply
		// message, _ := bufio.NewReader(conn).ReadString('\n')
		// fmt.Print("Message from server: "+message)
		// }


	}
}

func send(conn net.Conn, from string, to string, content string) {
	msg := NewMsg()
	msg.Header.Verb = MSGHEADER_VERB_CHAT
	// msg.Header.Verb = MSGHEADER_VERB_REGISTER
	msg.Body = MsgBody{make(map[string]string)}
	// msg.Body.Props["requester"] = from
	msg.Body.Props["from"] = from
	msg.Body.Props["to"] = to
	msg.Body.Props["content"] = content
	// msg.Body.Props["password"] = "111111"

	fmt.Fprintf(conn, msg.ToString())     // listen for reply
}

func dummyFunc(port int, hostName string) {

}

func receive(conn net.Conn) {
	reply := make([]byte, 1024)
	for {
		_, err := conn.Read(reply)
		if err != nil {
			println("Receiving failed:", err.Error())
			os.Exit(1)
		}

		println("reply from server=", string(reply))
	}
}
