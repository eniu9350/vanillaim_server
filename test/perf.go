package main

import(
  "net"
  . "realjin.info/vanillaim_lib"
  "fmt"
  "os"
)

func main(){
  conn, _ := net.Dial("tcp", fmt.Sprintf("%s:%d", "127.0.0.1", 5005))

  go receive(conn)

  //send login
  sendLogin(conn, "user1")
  for{
    
  }
}

func receive(conn net.Conn) {
	reply := make([]byte, 1024)
	for {
		_, err := conn.Read(reply)
		if err != nil {
			println("Receiving failed:", err.Error())
			os.Exit(1)
		}

		println("reply from server=", string(reply))
	}
}

func sendLogin(conn net.Conn, requester string) {
	msg := NewMsg()
	msg.Header.Verb = MSGHEADER_VERB_LOGIN
	msg.Body = MsgBody{make(map[string]string), make(map[string][]string)}
	msg.Body.Props["requester"] = requester
	fmt.Fprintf(conn, msg.ToString())     // listen for reply
}
