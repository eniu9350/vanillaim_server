package main

import  (
  domain "realjin.info/vanillaim_lib/domain"
  // "realjin.info/vanillaim_lib"
  "time"
  "net"
  "strings"
  "fmt"
  // "encoding/json"
)

/*
  manage lifecycle of session(creation, etc.)
*/

type SessionMgr struct {
  SessionMap map[string]*domain.Session
  ConnSessionMap map[string]*domain.Session
}


func NewSessionMgr() *SessionMgr {
	sessionMgr := SessionMgr{}
	sessionMgr.SessionMap = make(map[string]*domain.Session)
  sessionMgr.ConnSessionMap = make(map[string]*domain.Session)
	return &sessionMgr
}

//TODO
/*
  used in login/x
*/
func (sessionMgr *SessionMgr)createSession(qName domain.QName, conn net.Conn) {
  session := domain.Session{}
  //TODO: right?
  session.QName = qName
  session.Conn = conn
  now := time.Now().UnixNano() / 1000000
  session.CreateTime = now
  session.UpdateTime = now
  //TODO: dup check

  sessionMgr.SessionMap[qName.ToString()] = &session
  sessionMgr.ConnSessionMap[conn.RemoteAddr().String()] = &session
  fmt.Printf("[createSession]conn=%p", &conn)
}

func (sessionMgr *SessionMgr)getSession(qName domain.QName) *domain.Session {
  //TODO: right?
  if val, ok := sessionMgr.SessionMap[qName.ToString()]; ok {
    return val
  }else{
    return nil
  }
}

func (sessionMgr *SessionMgr)getSessionByUserName(userName string) *domain.Session {
  for k, v := range sessionMgr.SessionMap {
    //TODO: temp --- return single
    if strings.HasPrefix(k, userName+":") {
      return v
    }
  }
  return nil
}

func (sessionMgr *SessionMgr)getSessionByConn(conn net.Conn) *domain.Session {
  fmt.Printf("[getSessionByConn]conn=%p, localaddr=%s", conn, conn.LocalAddr().String())
  //TODO: right?
  if val, ok := sessionMgr.ConnSessionMap[conn.RemoteAddr().String()]; ok {
    return val
  }else{
    return nil
  }
}
