package main

import (
	// "log"
	"strconv"
	"net"
	log "github.com/Sirupsen/logrus"
	"github.com/rifflock/lfshook"
	rtdb "github.com/dancannon/gorethink"
	// . "realjin.info/vanillaim_lib"
	rtdao "realjin.info/vanillaim_lib/dao/rethinkdb"
	db "realjin.info/vanillaim_lib/db/rethinkdb"
	// dao "realjin.info/vanillaim_lib/dao"
)

var Log *log.Logger

// func NewLogger( config map[string]interface{} ) *log.Logger {
func NewLogger(cfg *ServerConfig) *log.Logger {
    if Log != nil {
        return Log
    }

    Log = log.New()
    Log.Formatter = new(log.JSONFormatter)
    Log.Hooks.Add(lfshook.NewHook(lfshook.PathMap{
        log.InfoLevel : cfg.Log.Info,
        log.ErrorLevel : cfg.Log.Error,
    }))
    return Log
}

const MSG_BUF_SIZE int = 2 * 128
const READ_BUF_SIZE int = 128

//TODO: not used yet
type Server struct {
	SessionMgr *SessionMgr
	DataMgr *DataMgr
}


func main() {
	// Log.Info("===Loading config===")
	cfg := ParseConfig()

	Log = NewLogger(cfg)

	Log.Info("===Creating SessionMgr===")
	sessionMgr := NewSessionMgr()

	//start api
	api := NewApi(cfg, sessionMgr)
	go api.Start()

	//start console
	con := NewConsole(cfg)
	go con.Start();



	mainLoop(cfg, sessionMgr)
}

/*============
the loop that accept connection and spawning goroutines that
do the decoding(L7 message parsing)
=============*/
func mainLoop(cfg *ServerConfig, sessionMgr *SessionMgr) {
	Log.Info("port="+strconv.Itoa(cfg.Server.Port))
	ln, err := net.Listen("tcp", ":"+strconv.Itoa(cfg.Server.Port))
	if err != nil {
		// log.Fatalf("listen error: %v", err)
		Log.Error("Listen error!")
	}

	Log.WithFields(log.Fields{
    "port": strconv.Itoa(cfg.Server.Port),
  }).Info("===Start listening===")



	Log.Info("===Establish db connection(persist db) and create dao===")
	//TODO: other types of db_persist
	var rtdbSession *rtdb.Session
	//establish rethinkdb connection
	rtdbSession, err = rtdb.Connect(rtdb.ConnectOpts{
		Address: cfg.Db.Address})  //188.166.253.239
	if err != nil {
		Log.Error(err.Error())
	}

	userDao := rtdao.NewUserDaoRethinkDB(rtdbSession)
	nonceDao := rtdao.NewNonceDaoRethinkDB(rtdbSession)
	friendshipDao := rtdao.NewFriendshipDaoRethinkDB(rtdbSession)

	Log.Info("===Init db(persist db)===")
	db.DbInitAutoIncValue("user", rtdbSession)
	db.DbInitAutoIncValue("nonce", rtdbSession)

	serviceHandler := NewDefaultServiceHandler(sessionMgr, &userDao, &nonceDao, &friendshipDao)
	accessHandler := NewAccessHandler(serviceHandler, sessionMgr) //TODO: obj to watch lifecycle of

	for {
		conn, err := ln.Accept()
		if err != nil {
			// log.Fatalf("accept error: %v", err)
			Log.Error("Accept error!")
		}

		Log.Info("Accepted, to handle")
		go accessHandler.handle(conn)
	}

}
