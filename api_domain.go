package main

type ApiDomainUser struct {
  UserId string;
  Username string;
  Password string;
  Status int;
}

type ApiDomainFriendship struct {
  UserName string;
  Friend string;
}
