package main

import (
	"fmt"
	"strings"
	r "github.com/dancannon/gorethink"
	"net"
	"time"
	// "strconv"
	"crypto/sha256"
	"encoding/base64"
	lib "realjin.info/vanillaim_lib"
	dao "realjin.info/vanillaim_lib/dao"
	domain "realjin.info/vanillaim_lib/domain"
)

type ServiceHandler interface	{
	handle(<-chan *MsgEvent)
}

type MsgEvent struct {
	Msg *lib.Msg
	Conn net.Conn
}


type DefaultServiceHandler struct {
	SessionMgr *SessionMgr
	UserDao dao.UserDao
	NonceDao dao.NonceDao
	FriendshipDao dao.FriendshipDao
}

func NewDefaultServiceHandler(_sessionMgr *SessionMgr, _userDao dao.UserDao,  _nonceDao dao.NonceDao, _friendshipDao dao.FriendshipDao) *DefaultServiceHandler {
	serviceHandler := DefaultServiceHandler{}
	serviceHandler.SessionMgr = _sessionMgr
	serviceHandler.UserDao = _userDao
	serviceHandler.NonceDao = _nonceDao
	serviceHandler.FriendshipDao = _friendshipDao
	return &serviceHandler
}

func (serviceHandler *DefaultServiceHandler) handle(msgchan <-chan *MsgEvent) {
	for {
		msgEvent := <-msgchan
		msg := msgEvent.Msg

		Log.Info("msg received from channel:["+msg.Header.Verb+"]")

		if msg.Header.Verb == lib.MSGHEADER_VERB_REG {
			serviceHandler.doRegister(msgEvent.Msg, msgEvent.Conn)
		} else if msg.Header.Verb == lib.MSGHEADER_VERB_LOGIN {
			serviceHandler.doLogin(msgEvent.Msg, msgEvent.Conn)
		} else if msg.Header.Verb == lib.MSGHEADER_VERB_CHAT {
			serviceHandler.doChat(msgEvent.Msg, msgEvent.Conn)
		} else if msg.Header.Verb == lib.MSGHEADER_VERB_HB {
			serviceHandler.doHeartBeat(msgEvent.Msg, msgEvent.Conn)
		} else if msg.Header.Verb == lib.MSGHEADER_VERB_LOGOUT {
			serviceHandler.doLogout(msgEvent.Msg, msgEvent.Conn)
		} else if msg.Header.Verb == lib.MSGHEADER_VERB_FADD {
			serviceHandler.doFriendshipAdd(msgEvent.Msg, msgEvent.Conn)
		} else if msg.Header.Verb == lib.MSGHEADER_VERB_FADD {
			serviceHandler.doFriendshipAdd(msgEvent.Msg, msgEvent.Conn)
		} else if msg.Header.Verb == lib.MSGHEADER_VERB_FADD_FIN{
			serviceHandler.doFriendshipAddFin(msgEvent.Msg, msgEvent.Conn)
		}	else{

			Log.Error("Unknown verb!")
		}

	}
}

func (serviceHandler *DefaultServiceHandler) doRegister(msg *lib.Msg, conn net.Conn) {
	Log.Info("[doRegister] msg=")
	msg.Show()

	requester := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_REQUESTER)
	password := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_PASSWORD)
	nickName := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_NICKNAME)

	//TODO: no devname parsing
	qName := domain.ParseQName(requester)

	err := serviceHandler.UserDao.UserCreate(qName.UserName, nickName, password)
	if err != nil	{
		Log.Error("UserCreate failed")
	}

	Log.Info("User created.")

	//TODO: pid related
	//TODO: temp!!!
	// connFrom := serviceHandler.ConnMgr.getRandomByUid(pid.Uid)

	//send response
	response := lib.NewMsg()
	response.Header.Verb = lib.MSGHEADER_VERB_REG_FIN
	response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_RESULT] = lib.MSGBODY_PROPS_VALUE_OF_RESULT_OK

	fmt.Fprintf(conn, response.ToString())

}

func (serviceHandler *DefaultServiceHandler) doLogin(msg *lib.Msg, conn net.Conn) {
	Log.Info("[doLogin] msg=")
	msg.Show()

	fmt.Printf("[doLogin] conn=%p", &conn)

	requesterStr := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_REQUESTER)
	nonce := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_NONCE)
	pswhash := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_PSWHASH)
	requester := domain.ParseQName(requesterStr)

	//prepare response
	response := lib.NewMsg()
	response.Header.Verb = lib.MSGHEADER_VERB_LOGIN_FIN

	Log.Info("requesterStr="+requesterStr)
	userGot, err := serviceHandler.UserDao.UserGet(requester.UserName)

	if err == nil {
		Log.Info("User got.")
		response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_NICKNAME] = userGot.Nickname

		if(len(nonce)==0)	{
			Log.Error("Nonce not exist or empty.")
			//generate nonce
			generatedNonce,_ := serviceHandler.NonceDao.NonceCreate(conn.RemoteAddr().String())
			//TODO check err

			response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_RESULT] = lib.MSGBODY_PROPS_VALUE_OF_RESULT_ERROR_LOGIN_NO_NONCE
			response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_NONCE] = generatedNonce
		} else {
			//TODO: check expiration
			storedNonce, _:=serviceHandler.NonceDao.NonceGet(conn.RemoteAddr().String())
			//TODO: check err
			Log.Info("storedNonce="+storedNonce+",nonce="+nonce)
			if storedNonce==nonce	{
				Log.Info("nonce matched!")
				// x.invalidateNonce(conn, nonce)

				//check pswhash
				rawHash := sha256.Sum256([]byte(userGot.Username+"|"+userGot.Password+"|"+nonce))
				generatedHash := base64.StdEncoding.EncodeToString([]byte(rawHash[:]))	//TODO: "res :=  " and check resp

				Log.Info("generatedHash="+generatedHash+", pswhash="+pswhash)

				if generatedHash == pswhash {	//password matched
					Log.Info("Password check passed.")
					response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_RESULT] = lib.MSGBODY_PROPS_VALUE_OF_RESULT_OK

					//TODO:1. [***] make sure what I want to check!
					//TODO:2. policy: troll replace?
					//update session
					session := serviceHandler.SessionMgr.getSession(*requester)

					//TODO: ned
					if session == nil	{
						serviceHandler.SessionMgr.createSession(*requester, conn)
						Log.Info("Session created.")
					}	else {
						Log.Info("Session already exist.")
						//e.g. when disconnected unexpectedly and reconnect while original session have not expired yet
						//TODO abnormal case?
						now := time.Now().UnixNano() / 1000000
						session.CreateTime = now
						session.UpdateTime = now
					}

					//update presence
					//TODO
					// lib.DbopPresenceUpdate(serviceHandler.DataMgr.RedisConn, pid.Uid, lib.PRESENCE_NOTOFFLINE_EXT_AVAILABLE)
				}else	{	//password not matched
					Log.Error("Wrong password, login failed.")
					//TODO: update nonce
					response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_RESULT] = lib.MSGBODY_PROPS_VALUE_OF_RESULT_ERROR_LOGIN_WRONG_PSWHASH
					response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_NONCE] = "itsasamplenonce"
				}
			} else {//nonce not valid
				//TODO: nonce not valid
			}
		}

	} else if err == r.ErrEmptyResult {
		Log.Error("Result not found,login failed.")
		response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_RESULT] = lib.MSGBODY_PROPS_VALUE_OF_RESULT_ERROR_LOGIN_USER_NOT_EXIST
		// response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_NONCE] = "itsasamplenonce"
	} else {
		// error
		fmt.Print(err)
	}

	//send response
	fmt.Fprintf(conn, response.ToString())

	//send friendship information(full)
	flistMsg := lib.NewMsg()
	flistMsg.Header.Verb = lib.MSGHEADER_VERB_FLIST
	flistMsg.Body.MVProps[lib.MSGBODY_MVPROPS_BASIC_KEY_MEMBER] = make([]string, 0, 50)

	friendships, _ := serviceHandler.FriendshipDao.FriendshipGetAll(requester.UserName)
	for _,friendship := range friendships	{
		userAttrList := make([]string,0,10)
		userAttrList = append(userAttrList, "username="+friendship.UserName)
		userAttrList = append(userAttrList, "friend="+friendship.Friend)
		userAttrList = append(userAttrList, "friendnickname="+friendship.FriendNickname)
		propValue := strings.Join(userAttrList, "|")

		flistMsg.Body.MVProps[lib.MSGBODY_MVPROPS_BASIC_KEY_MEMBER] = append(flistMsg.Body.MVProps[lib.MSGBODY_MVPROPS_BASIC_KEY_MEMBER], propValue)
	}

	fmt.Fprintf(conn, flistMsg.ToString())
}

func (serviceHandler *DefaultServiceHandler) doLogout(msg *lib.Msg, conn net.Conn) {
	Log.Info("[doLogout] msg=")
	msg.Show()

	//TODO missing logic
	// requester := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_REQUESTER)
	// pid := lib.PidParseString(requester)

	//update presence
	//TODO
	// lib.DbopPresenceUpdate(serviceHandler.DataMgr.RedisConn, pid.Uid, lib.PRESENCE_OFFLINE_OFFLINE)

	//TODO: pid related
	//TODO: temp!!!
	// connFrom := serviceHandler.ConnMgr.getRandomByUid(pid.Uid)

	// requester := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_REQUESTER)

	//send response
	response := lib.NewMsg()
	response.Header.Verb = lib.MSGHEADER_VERB_LOGOUT_FIN
	//TODO: use SetProp
	response.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_RESULT] = lib.MSGBODY_PROPS_VALUE_OF_RESULT_OK

	fmt.Fprintf(conn, response.ToString())
}

func (serviceHandler *DefaultServiceHandler) doChat(msg *lib.Msg, conn net.Conn) {
	Log.Info("[doChat] msg=")
	msg.Show()
	fmt.Printf("[doChat] conn=%p", &conn)

	to := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_TO)
	//	user := NewUserFromString(to)
	//TODO: [***] make sure what I want to check!
	// session := serviceHandler.SessionMgr.getSession(*domain.ParseQName(to))
	session := serviceHandler.SessionMgr.getSessionByUserName(domain.ParseQName(to).UserName)
	fromSession := serviceHandler.SessionMgr.getSessionByConn(conn)
	if fromSession == nil	{
		Log.Info("[doChat] fromSession nil")
	}	else	{
		Log.Info("[doChat] fromSession not nil")
	}

	//TODO: check if "from" field exist(exception)
	//TODO: use SetProp
	//append from information
	msg.Body.Props[lib.MSGBODY_PROPS_BASIC_KEY_FROM] = fromSession.QName.ToString()

	if session == nil {
		Log.Error("[doChat]Nil session.")
	} else {
		Log.Info("[doChat]Session got.")
		if session.Conn == nil	{
			Log.Info("[doChat]Nil connection found in session.")
		}else{
			fmt.Fprintf(session.Conn, msg.ToString())
		}
	}
}

func (serviceHandler *DefaultServiceHandler) doHeartBeat(msg *lib.Msg, conn net.Conn) {
	// Log.Info("[doHeartBeat] msg=")
	// msg.Show()

	strRequester := msg.GetProp(lib.MSGBODY_PROPS_BASIC_KEY_REQUESTER)
	requester := domain.ParseQName(strRequester)
	session := serviceHandler.SessionMgr.getSession(*requester)

	if session == nil {
		Log.Error("[doHeartBeat]Nil session.")
	} else {
		// Log.Info("[doHeartBeat]Session got.")
		if session.Conn == nil	{
			Log.Error("[doHeartBeat]Nil connection found in session.")
		}else{
			//TODO: check conn
			now := time.Now().UnixNano() / 1000000

			// for _, v := range serviceHandler.SessionMgr.SessionMap {
		    // Log.Info("==[session entry]== n="+v.QName.UserName+", ct="+strconv.FormatInt(v.CreateTime,10)+", ut="+strconv.FormatInt(v.UpdateTime,10))
		  // }

			session.UpdateTime = now
		}
	}
}
