package main

import (
	"log"
	"net"
	. "realjin.info/vanillaim_lib"
	// domain "realjin.info/vanillaim_lib/domain"
	//	"sync"
)

type AccessHandler struct {
	// Server         *Server
	ServiceHandler ServiceHandler
	SessionMgr *SessionMgr
	// DataMgr *DataMgr
}

func NewAccessHandler(_serviceHandler ServiceHandler, _sessionMgr *SessionMgr) AccessHandler {
	accessHandler := AccessHandler{}
	accessHandler.SessionMgr = _sessionMgr
	accessHandler.ServiceHandler = _serviceHandler
	//TODO: right to let member ConnMgr and ServiceMgr init in ad hoc way?
	//	accessHandler.ConnMgr = NewConnMgr()
	return accessHandler
}

func (accessHandler *AccessHandler) handle(conn net.Conn) {
	buf := make([]byte, MSG_BUF_SIZE)
	stream := make([]byte, READ_BUF_SIZE)

	//TODO: right to init here?
	msgEventChan := make(chan *MsgEvent)
	go accessHandler.ServiceHandler.handle(msgEventChan)

	dataInBuf := buf[0:0]
	for {
		n, err := conn.Read(stream)
		//		log.Printf("reading, dataInBuf len/cap=%d/%d, bytes read=%d", len(dataInBuf), cap(dataInBuf), n)
		if err != nil {
			return
		}
		// conn.Write([]byte(line))
		// msgchan <- string(buf[0:n])

		if n+len(dataInBuf) <= cap(buf) { //if len of stream + len of data in buf < cap of buf, append to dataInBuf
			//expand
			dataInBufAppended := buf[len(dataInBuf):(n + len(dataInBuf))]
			dataInBuf = buf[0:(n + len(dataInBuf))]

			//append
			//			log.Printf("expanded dataInBuf: len/cap=%d/%d", len(dataInBuf), cap(dataInBuf))
			//			log.Printf("buffer space enough, appending...")
			//			ret := copy(dataInBufAppended, stream)
			copy(dataInBufAppended, stream)
			//			log.Printf("appended %d bytes", ret)
		}
		//else //if larger than buf, "pop"(copy and shrink)

		//"pop"(copy and shrink)

		// log.Printf("buffer space not enough to accomodate new message(dataInBuf len=%d), popping... ", len(dataInBuf))
		i := 0
		for i < len(dataInBuf)-3 {

			for i = 0; i < len(dataInBuf)-3; i++ {
				if dataInBuf[i] == '\r' && dataInBuf[i+1] == '\n' && dataInBuf[i+2] == '\r' && dataInBuf[i+3] == '\n' {
					//						log.Printf("message end found")
					//following: pop and shrink dataInBuf (note that dataInBuf always share the same start addrss with buf)

					//pop data of dataInBuf[:i+4]
					popped := make([]byte, i+3+1)
					copy(popped, dataInBuf) //TODO: mechanism

					//parse
					parsedMsg := accessHandler.parse(popped)
					msgEvent := MsgEvent{&parsedMsg, conn}


					//handle in service handler
					//TODO: right?
					msgEventChan <- &msgEvent

					// decodedMsg.show()
					//copy data from dataInBuf[i+4:xx] to dataInBuf[:x]
					newLenOfDataInBuf := len(dataInBuf) - (i + 3 + 1)
					byteSliceCopyInternalBytes(dataInBuf, 0, i+4, newLenOfDataInBuf) //mmm: len>?
					//shrink
					dataInBuf = dataInBuf[:newLenOfDataInBuf]
					break
				}
				//pop
			}
		} //end outmost for

		//if not found
		if n+len(dataInBuf) > cap(buf) {
			log.Fatalf("Not enough space in buffer after popping up all the messages!")
		}
	}	//for end
}

func (accessHandler *AccessHandler) parse(s []byte) Msg {
	// log.Printf("[parse]start, len of s=%d", len(s))
	//	log.Printf("[decode]msg content=%s", string(s))

	msg := NewMsg()
	// msg.Body = MsgBody{make(map[string]string)}

	n := 0
	start := 0
	i := 0
	for i < len(s)-2 {
		if s[i] == '\r' && s[i+1] == '\n' {
			//copy to verb or map
			if n == 0 {
				if s[start] == '[' && s[i-1] == ']' {
					//copy to verb
					msg.Header = MsgHeader{string(s[start+1 : i-1])}
				} else {
					log.Fatalf("[parse]header pass error!")
				}
			} else {
				//copy to map
				var j int
				for j = start; j < i; j++ {
					if s[j] == ' ' {
						break
					}
				}

				//TODO check j
				msg.Body.Props[string(s[start:j-1])] = string(s[j+1 : i])
			}

			n++
			start = i + 2
			i += 2
		} else {
			i++
		}
	}

	//	msg.Show()
	// log.Printf("[parse]end")

	//TODO: Q/A - what does this mean
	return msg
}

//dst smaller than src
func byteSliceCopyInternalBytes(s []byte, dstStart int, srcStart int, len int) {
	//TODO: no check
	for i := 0; i < len; i++ {
		s[dstStart+i] = s[srcStart+i]
	}
}
